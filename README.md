Introduction
============
This is a series of problem sets in the style of Google Code Jam (parse inputfile and produce output) intended to teach python to beginners. 

The philosophy of this is that trying to solve problems on your own leads an effective and more immersive learning experience,

Each directory contains a `question.txt` and an `input .txt`. The goal is to create an `output.txt` according to the specifcations in `question.txt`

Directory Tree
==============

    python-jam/
    ├── 01
    │   ├── input.txt
    │   ├── question.txt
    └── README.md
    
    1 directory, 3 files